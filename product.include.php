    <!-- Include jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
    <!-- Include Zoom Ganteng JavaScript -->
    <script type="text/javascript" src="assets/zoomganteng/zoomganteng.js"></script>
    <!-- Include Thumbcantik JavaScript -->
    <script type="text/javascript" src="assets/zoomganteng/thumbcantik.js"></script>
    <!-- Include Zoom Ganteng CSS -->
    <link href="assets/zoomganteng/zoomganteng.css" type="text/css" rel="stylesheet" />
    <!-- Include Thumbcantik CSS -->
    <link href="assets/zoomganteng/thumbcantik.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
    CloudZoom.quickStart(); 

    // Initialize the slider.    
    $(function() {      
    $.noConflict();  
        $('#slider-produk').Thumbelina({            
            $bwdBut: $('#slider-produk .left'),
            $fwdBut: $('#slider-produk .right')
        });
    });
    
    </script>
    <style>
    /* div that surrounds Zoom Ganteng image and content slider. */
    
    #surround {
        width: 50%;
        min-width: 256px;
        max-width: 300px;
        border: 1px solid #ccc;
    }
    /* Image expands to width of surround */
    
    img.cloudzoom {
        width: 100%;
    }
    /* CSS for slider - will expand to width of surround */
    
    #slider-produk {
        margin-left: 20px;
        margin-right: 20px;
        height: 119px;
        border-top: 1px solid #aaa;
        border-bottom: 1px solid #aaa;
        position: relative;
    }
    </style>