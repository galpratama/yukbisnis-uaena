<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Tentang Kami</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	 
	<!-- *****************************************************************************************************************
	 AGENCY ABOUT
	 ***************************************************************************************************************** -->
<div class="container mtb">
	 	<div class="row">
	 	<div class="col-lg-12">
	 		<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#info" data-toggle="tab">Info</a></li>
			  <li><a href="#relasi" data-toggle="tab">Relasi</a></li>
			  <li><a href="#status" data-toggle="tab">Status</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane fade in active" id="info">
			  		<div class="row">
			  		<br>
				 		<div class="col-lg-8">
				 		
					 		<p>
					 			Manéhna miboga saurang anak lalaki anu ngaranna Sangkuriang. Anak kasebut pohara gemar moro di jero leuweung. Saban moro, manéhna sok dibaturan ku sabuntut anjing kesayangannya anu ngaranna Tumang. Tumang sabenerna nyaéta titisan déwa, sarta ogé bapa kandung Sangkuriang, tapi Sangkuriang henteu weruh hal éta sarta indungna memang ngahaja merahasiakannya.
					 		</p>
							<p>
								Sangkuriang mimitian néangan buruan. Manéhna nempo aya sabuntut manuk anu keur bertengger di dahan, tuluy tanpa mikir panjang Sangkuriang langsung némbakna, sarta pas ngeunaan sasaran. Manéhna nempo aya sabuntut manuk anu keur bertengger di dahan, tuluy tanpa mikir panjang Sangkuriang langsung némbakna, sarta pas ngeunaan sasaran. 
							</p>
							<p>
								Sangkuriang mimitian néangan buruan. Manéhna nempo aya sabuntut manuk anu keur bertengger di dahan, tuluy tanpa mikir panjang Sangkuriang langsung némbakna, sarta pas ngeunaan sasaran. Manéhna nempo aya sabuntut manuk anu keur bertengger di dahan, tuluy tanpa mikir panjang Sangkuriang langsung némbakna, sarta pas ngeunaan sasaran. 
							</p>
							<p><br/><a href="contact.html" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Kontak Kami</a></p>
				 		</div>
				 		<div class="col-lg-4">
				 			<img class="img-responsive" src="assets/img/iu.jpg" alt="">
				 		</div>		
	 				</div><! --/row -->
			  </div>
			  <div class="tab-pane fade" id="relasi">
			 	 <div class="row">
			  		  <br>
					  <div class="col-lg-12">
					  	
<div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        
		                    </div>
		                </div>
		            </div>

					  </div>
				  </div>
			  </div>
			  <div class="tab-pane fade" id="status">
			  	  <div class="row">
			  		  <br>
					  <div class="col-lg-12">
						<div class="container">
						    <div class="qa-message-list" id="wallmessages">
						    				<div class="message-item" id="m16">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">Oleg Kolesnichenko</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Jan 21</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko">Oleg Kolesnichenko</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Yo!
													</div>
											</div></div>
											
											<div class="message-item" id="m9">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=amiya"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">amiya</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Nov 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=amiya">amiya</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Nice theme . Excellent one .
													</div>
											</div></div>
											
											<div class="message-item" id="m7">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=russell"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">russell</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 25, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=russell">russell</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Nullam porta leo vitae ipsum feugiat viverra. In sed placerat mi. Nullam euismod, quam in euismod rhoncus, tellus velit posuere tortor, non cursus nunc velit et lacus.
													</div>
											</div></div>
											
											<div class="message-item" id="m6">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=juggornot"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">juggornot</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=juggornot">juggornot</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Integer vitae arcu vitae ligula Cras vestibulum suscipit odio ac dapibus. In hac habitasse platea dictumst. Cras pulvinar erat et nunc fringilla, quis molestie
													</div>
											</div></div>
											
											<div class="message-item" id="m5">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=one_eyed"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">one_eyed</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=one_eyed">one_eyed</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Nulla dui ante, pulvinar ac auctor vitae, sollicitudin et tortor. Cras vestibulum suscipit odio ac dapibus. In hac habitasse platea dictumst. Cras pulvinar erat et nunc fringilla, quis molestie diam pulvinar.
													</div>
											</div></div>
											
											<div class="message-item" id="m4">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=muboy"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">muboy</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=muboy">muboy</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fermentum iaculis mi, non dapibus nulla eleifend sed. Etiam ac commodo leo. <br>
						Donec non sem id tellus mattis convallis. Morbi dapibus nulla ac dui lacinia,
													</div>
											</div></div>
											
											<div class="message-item" id="m3">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=monu"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">monu</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=monu">monu</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Suspendisse varius mi consectetur nulla volutpat, nec fermentum turpis vehicula. Curabitur dapibus odio mauris, vitae accumsan sapien auctor non. Duis tempus ante id nulla vestibulum mattis.
													</div>
											</div></div>
											
											<div class="message-item" id="m2">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=Fynn"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">Fynn</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=Fynn">Fynn</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Nunc ante neque, feugiat at dictum ut, dignissim sed sapien. Pellentesque congue eu nisl sit amet cursus. Integer dapibus adipiscing metus ac vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</div>
											</div></div>
											
											<div class="message-item" id="m1">
												<div class="message-inner">
													<div class="message-head clearfix">
														<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=admin"><img src="https://ssl.gstatic.com/accounts/ui/avatar_1x.png"></a></div>
														<div class="user-detail">
															<h5 class="handle">admin</h5>
															<div class="post-meta">
																<div class="asker-meta">
																	<span class="qa-message-what"></span>
																	<span class="qa-message-when">
																		<span class="qa-message-when-data">Oct 24, 2013</span>
																	</span>
																	<span class="qa-message-who">
																		<span class="qa-message-who-pad">by </span>
																		<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=admin">admin</a></span>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="qa-message-content">
														Nunc ante neque, feugiat at dictum ut, dignissim sed sapien. Pellentesque congue eu nisl sit amet cursus. Integer dapibus adipiscing metus ac vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fermentum iaculis mi, non dapibus nulla eleifend sed. Etiam ac commodo leo. Donec non sem id tellus mattis convallis. Morbi dapibus nulla ac dui lacinia,
													</div>
											</div></div>
											
										</div>
						</div>



					  </div>
				  </div>
			  </div>
			</div>
	 	</div>		
	 	</div><! --/row -->
	 	<br>
	 	
	 </div><! --/container -->
<?php include "footer.php" ?>