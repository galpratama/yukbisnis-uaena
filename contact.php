<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Kontak Kami</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	<!-- *****************************************************************************************************************
	 CONTACT WRAP
	 ***************************************************************************************************************** -->

	 <div id="contactwrap"></div>
	 
	<!-- *****************************************************************************************************************
	 CONTACT FORMS
	 ***************************************************************************************************************** -->
	<br>
	 <div class="container mtb">
	 	<div class="row">
	 		<div class="col-lg-8">
	 			<h3>Ada Yang Ingin Ditanyakan?</h3>
	 			<div class="hline"></div>
		 			<p>Léos Si Kabayan leumpang ka sawah nu meunang ngagaru. Di dinya katémbong tututna loba.</p>
		 			<form role="form">
					  <div class="form-group">
					    <label for="InputName1">Nama Anda</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					    <label for="InputEmail1">Alamat Email</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					    <label for="InputSubject1">Subyek</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					  	<label for="message1">Pesan</label>
					  	<textarea class="form-control" id="message1" rows="3"></textarea>
					  </div>
					  <button type="submit" class="btn btn-theme">Kirim</button>
					</form>
			</div><! --/col-lg-8 -->
	 		
	 		<div class="col-lg-4">
		 		<h3>Alamat Kami</h3>
		 		<div class="hline"></div>
		 			<p>
		 				Jl. Braga No.99-101, <br/>
			            Bandung, 40111, <br/>
			            Indonesia <br/>
		 			</p>
		 			<p>
		 				Email: hello@bragastudio.co<br/>
		 				Tel: 0857-1010-4467
		 			</p>
		 			<p>Léos Si Kabayan leumpang ka sawah nu meunang ngagaru. Di dinya katémbong tututna loba.</p>
	 		</div>
	 	</div><! --/row -->
	 </div><! --/container -->

<?php include "footer.php" ?>