<?php include "header.php"; ?>

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Blog</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	 
	<!-- *****************************************************************************************************************
	 BLOG CONTENT
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row">
	 	
	 		<! -- SINGLE POST -->
	 		<div class="col-lg-8">
	 			<! -- Blog Post 1 -->
		 		<a href="single-post.php"><h2 class="ctitle">Hamburger tri-tip sausage doner</h2></a>
				<p><csmall>Ditulis: 17 Agustus 2014 Oleh Galih Pratama - 3 Komentar</csmall></p>
		 		<p><img class="img-responsive" src="assets/img/post02.jpg"></p>
				<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
				<p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>
				<p>Completely synergize resource sucking relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.</p>
				<h4>Corporation Synergy</h4>
				<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.</p>
				<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.</p>
				<blockquote>Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.</blockquote>
				<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.</p>
		 		<div class="spacing"></div>
		 		<h6>BAGIKAN:</h6>
		 		<p class="share">
		 			<a href="#"><i class="fa fa-twitter"></i></a>
		 			<a href="#"><i class="fa fa-facebook"></i></a>
		 			<a href="#"><i class="fa fa-tumblr"></i></a>
		 			<a href="#"><i class="fa fa-google-plus"></i></a>		 		
		 		</p>
		 		
			</div><! --/col-lg-8 -->
	 		
	 		
	 		<! -- SIDEBAR -->
	 		<div class="col-lg-4">
	 			<p>
	 				<div class="input-group">
			          <input type="text" class="form-control" placeholder="Cari Artikel ...">
			          <div class="input-group-btn">
			            <button type="button" class="btn btn-default" tabindex="-1"><i class="fa fa-search"></i></button>
			          </div>
			        </div>
	 				<br>
	 			</p>

		 		<h4>Artikel Terbaru</h4>
		 		<div class="hline"></div>
					<ul class="popular-posts">
		                <li>
		                    <a href="#"><img src="assets/img/thumb01.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 02/21/14</em>
		                </li>
		                <li>
		                    <a href="#"><img src="assets/img/thumb02.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 03/01/14</em>
		                <li>
		                    <a href="#"><img src="assets/img/thumb03.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 05/16/14</em>
		                </li>
		                <li>
		                    <a href="#"><img src="assets/img/thumb04.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 05/16/14</em>
		                </li>
		            </ul>
		            
		 		<div class="spacing"></div>
		 		
		 		<h4>Kategori</h4>
		 		<div class="hline"></div>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Wordpress</a> <span class="badge badge-theme pull-right">9</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Photoshop</a> <span class="badge badge-theme pull-right">3</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Web Design</a> <span class="badge badge-theme pull-right">11</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Development</a> <span class="badge badge-theme pull-right">5</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Tips & Tricks</a> <span class="badge badge-theme pull-right">7</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Code Snippets</a> <span class="badge badge-theme pull-right">12</span></p>

		 		<div class="spacing"></div>
		 		
		 		<h4>Tag</h4>
		 		<div class="hline"></div>
		 			<p>
		            	<a class="btn btn-link" href="#" role="button">Design</a>
		            	<a class="btn btn-link" href="#" role="button">Wordpress</a>
		            	<a class="btn btn-link" href="#" role="button">Flat</a>
		            	<a class="btn btn-link" href="#" role="button">Modern</a>
		            	<a class="btn btn-link" href="#" role="button">Wallpaper</a>
		            	<a class="btn btn-link" href="#" role="button">php5</a>
		            	<a class="btn btn-link" href="#" role="button">Pre-processor</a>
		            	<a class="btn btn-link" href="#" role="button">Developer</a>
		            	<a class="btn btn-link" href="#" role="button">Windows</a>
		            	<a class="btn btn-link" href="#" role="button">Phothosop</a>
		            	<a class="btn btn-link" href="#" role="button">UX</a>
		            	<a class="btn btn-link" href="#" role="button">Interface</a>		            	
		            	<a class="btn btn-link" href="#" role="button">UI</a>		            	
		            	<a class="btn btn-link" href="#" role="button">Blog</a>		            	
		 			</p>
	 		</div>
	 	</div><! --/row -->
	 </div><! --/container -->

<?php include "footer.php"; ?>