
  <!-- *****************************************************************************************************************
   FOOTER
   ***************************************************************************************************************** -->
   <div id="footerwrap">
    <div class="container">
      <div class="row">
       <div class="col-lg-3">
          <h4>Tentang</h4>
          <div class="hline-w"></div>
          <p>Chambray minimal beauty 90s rose gold leather Chambray minimal beauty 90s rose gold leather tote denim shorts Topshop Paris Saint Laurent Cara D. Copenhagen dove grey leggings grunge Lanvin. </p>
        </div>
        <div class="col-lg-3">
          <h4>Alamat Kami</h4>
          <div class="hline-w"></div>
          <p>
            Jl. Braga No.99-101,<br/>
            Bandung, 40111,<br/>
            Indonesia<br/>
            0857-1010-4467
          </p>
        </div>
        <div class="col-lg-3">
          <h4>Media Sosial</h4>
          <div class="hline-w"></div>
          <p>
            <ul>
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Instagram</a></li>
            </ul>
          </p>
        </div>
        <div class="col-lg-3">
          <h4>Newsletter</h4>
          <div class="hline-w"></div>
              <p>Subscribe to our newsletter and get exclusive deals straight to your inbox!</p>
              <form>
                <input type="email" class="form-control" name="email" placeholder="Your Email : "><br>
                <input type="submit" class="btn btn-primary" value="Subscribe">
              </form>
        </div>

      
      </div><! --/row -->
    </div><! --/container -->
   </div><! --/footerwrap -->
   <div id="footerbottom">
     <p class="text-center">&copy; Uaena Store</p>
   </div>
   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/retina-1.1.0.js"></script>
    <script src="assets/js/jquery.isotope.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/jquery.bootstrap-touchspin.js"></script>
    <script src="assets/js/smoothproducts.min.js"></script>
    <script type="text/javascript">
        /* wait for images to load */
        $(window).load( function() {
            $('.sp-wrap').smoothproducts();
        });
    </script>
    <script>
      $("input[name='demo3_21']").TouchSpin({
          initval: 1
      });
    </script>
    <script>
      $('.carousel').carousel({
        interval: 10000
      })
    </script>
  </body>
</html>
