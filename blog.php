<?php include "header.php"; ?>

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Blog</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	 
	<!-- *****************************************************************************************************************
	 BLOG CONTENT
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row">

	 		<! -- BLOG POSTS LIST -->
	 		<div class="col-lg-8">
	 			<! -- Blog Post 1 -->
		 		<a href="single-post.php"><h2 class="ctitle">Pastrami shank filet mignon spare</h2></a>
				<p><csmall>Ditulis: 17 Agustus 2014 Oleh Galih Pratama - 3 Komentar</csmall></p>
		 		<p><img class="img-responsive" src="assets/img/post01.jpg"></p>
				<p>Dina jaman baheula, di Jawa Kulon hiduplah saurang putri raja anu ngaranna Dayang Sumbi. Man&eacute;hna miboga saurang anak lalaki anu ngaranna Sangkuriang. Anak kasebut pohara gemar moro di jero leuweung. Saban moro, man&eacute;hna sok dibaturan ku sabuntut anjing kesayangannya anu ngaranna Tumang.</p>
				<p>Tumang sabenerna nya&eacute;ta titisan d&eacute;wa, sarta og&eacute; bapa kandung Sangkuriang, tapi Sangkuriang henteu weruh hal &eacute;ta sarta indungna memang ngahaja merahasiakannya.</p>
		 		<p><a class="btn btn-block btn-primary" href="single-post.php">Baca Selengkapnya</a></p>
		 		<div class="hline"></div>
		 		
		 		<div class="spacing"></div>
		 		
	 			<! -- Blog Post 2 -->
		 		<a href="single-post.php"><h2 class="ctitle">Hamburger tri-tip sausage doner</h2></a>
				<p><csmall>Ditulis: 17 Agustus 2014 Oleh Galih Pratama - 3 Komentar</csmall></p>
		 		<p><img class="img-responsive" src="assets/img/post02.jpg"></p>
				<p>Dina jaman baheula, di Jawa Kulon hiduplah saurang putri raja anu ngaranna Dayang Sumbi. Man&eacute;hna miboga saurang anak lalaki anu ngaranna Sangkuriang. Anak kasebut pohara gemar moro di jero leuweung. Saban moro, man&eacute;hna sok dibaturan ku sabuntut anjing kesayangannya anu ngaranna Tumang.</p>
				<p>Tumang sabenerna nya&eacute;ta titisan d&eacute;wa, sarta og&eacute; bapa kandung Sangkuriang, tapi Sangkuriang henteu weruh hal &eacute;ta sarta indungna memang ngahaja merahasiakannya.</p>
		 		<p><a class="btn btn-block btn-primary" href="single-post.php">Baca Selengkapnya</a></p>
		 		<div class="hline"></div>

		 		<div class="spacing"></div>
		 		
	 			<! -- Blog Post 3 -->
		 		<a href="single-post.php"><h2 class="ctitle">Kevin frankfurter tongue tri-tip</h2></a>
				<p><csmall>Ditulis: 17 Agustus 2014 Oleh Galih Pratama - 3 Komentar</csmall></p>
		 		<p><img class="img-responsive" src="assets/img/post03.jpg"></p>
				<p>Dina jaman baheula, di Jawa Kulon hiduplah saurang putri raja anu ngaranna Dayang Sumbi. Man&eacute;hna miboga saurang anak lalaki anu ngaranna Sangkuriang. Anak kasebut pohara gemar moro di jero leuweung. Saban moro, man&eacute;hna sok dibaturan ku sabuntut anjing kesayangannya anu ngaranna Tumang.</p>
				<p>Tumang sabenerna nya&eacute;ta titisan d&eacute;wa, sarta og&eacute; bapa kandung Sangkuriang, tapi Sangkuriang henteu weruh hal &eacute;ta sarta indungna memang ngahaja merahasiakannya.</p>
		 		<p><a class="btn btn-block btn-primary" href="single-post.php">Baca Selengkapnya</a></p>
		 		<div class="hline"></div>
		 		
			</div><! --/col-lg-8 -->
	 		
	 		
	 		<! -- SIDEBAR -->
	 		<div class="col-lg-4">
	 			<p>
	 				<div class="input-group">
			          <input type="text" class="form-control" placeholder="Cari Artikel ...">
			          <div class="input-group-btn">
			            <button type="button" class="btn btn-default" tabindex="-1"><i class="fa fa-search"></i></button>
			          </div>
			        </div>
	 				<br>
	 			</p>

		 		<h4>Artikel Terbaru</h4>
		 		<div class="hline"></div>
					<ul class="popular-posts">
		                <li>
		                    <a href="#"><img src="assets/img/thumb01.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 02/21/14</em>
		                </li>
		                <li>
		                    <a href="#"><img src="assets/img/thumb02.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 03/01/14</em>
		                <li>
		                    <a href="#"><img src="assets/img/thumb03.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 05/16/14</em>
		                </li>
		                <li>
		                    <a href="#"><img src="assets/img/thumb04.jpg" alt="Popular Post"></a>
		                    <p><a href="#">Léos Si Kabayan leumpang ka sawah nu meunang ngagaru</a></p>
		                    <em>Ditulis pada 05/16/14</em>
		                </li>
		            </ul>
		            
		 		<div class="spacing"></div>
		 		
		 		<h4>Kategori</h4>
		 		<div class="hline"></div>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Wordpress</a> <span class="badge badge-theme pull-right">9</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Photoshop</a> <span class="badge badge-theme pull-right">3</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Web Design</a> <span class="badge badge-theme pull-right">11</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Development</a> <span class="badge badge-theme pull-right">5</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Tips & Tricks</a> <span class="badge badge-theme pull-right">7</span></p>
			 		<p><a href="#"><i class="fa fa-angle-right"></i> Code Snippets</a> <span class="badge badge-theme pull-right">12</span></p>

		 		<div class="spacing"></div>
		 		
		 		<h4>Tag</h4>
		 		<div class="hline"></div>
		 			<p>
		            	<a class="btn btn-link" href="#" role="button">Design</a>
		            	<a class="btn btn-link" href="#" role="button">Wordpress</a>
		            	<a class="btn btn-link" href="#" role="button">Flat</a>
		            	<a class="btn btn-link" href="#" role="button">Modern</a>
		            	<a class="btn btn-link" href="#" role="button">Wallpaper</a>
		            	<a class="btn btn-link" href="#" role="button">php5</a>
		            	<a class="btn btn-link" href="#" role="button">Pre-processor</a>
		            	<a class="btn btn-link" href="#" role="button">Developer</a>
		            	<a class="btn btn-link" href="#" role="button">Windows</a>
		            	<a class="btn btn-link" href="#" role="button">Phothosop</a>
		            	<a class="btn btn-link" href="#" role="button">UX</a>
		            	<a class="btn btn-link" href="#" role="button">Interface</a>		            	
		            	<a class="btn btn-link" href="#" role="button">UI</a>		            	
		            	<a class="btn btn-link" href="#" role="button">Blog</a>		            	
		 			</p>
	 		</div>
	 	</div><! --/row -->
	 </div><! --/container -->


<?php include "footer.php"; ?>