  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>Uaena Fashions - Beauty and Smiles</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="style-default.css" rel="stylesheet"> -->
    <!-- <link href="style-purple.css" rel="stylesheet"> -->
    <link href="style-brown.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/jquery.bootstrap-touchspin.css" rel="stylesheet">
    <link href="assets/css/smoothproducts.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    
  </head>

  <body>


    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">❤ Uaena Fashions</a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
          <ul class="nav navbar-nav">
          <form class="navbar-form navbar-left">
            <input type="text" class="form-control col-lg-8" placeholder="Search">
          </form>
          <li><a href="index.php">BERANDA</a></li>
          <li class="dropdown mega-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">KATEGORI <i class="fa fa-chevron-down"></i></a>
                <ul class="dropdown-menu mega-dropdown-menu row">
                  <li class="col-sm-3">
                    <ul>
                      <li class="dropdown-header">New in Stores</li>                            
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          <div class="carousel-inner">
                            <div class="item active">
                                <a href="catalogue.php"><img src="http://placehold.it/254x150/3498db/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 1"></a>
                                <h4><small>Summer dress floral prints</small></h4>                                        
                                <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                            </div><!-- End Item -->
                            <div class="item">
                                <a href="catalogue.php"><img src="http://placehold.it/254x150/ef5e55/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 2"></a>
                                <h4><small>Gold sandals with shiny touch</small></h4>                                        
                                <button class="btn btn-primary" type="button">9,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>        
                            </div><!-- End Item -->
                            <div class="item">
                                <a href="catalogue.php"><img src="http://placehold.it/254x150/2ecc71/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 3"></a>
                                <h4><small>Denin jacket stamped</small></h4>                                        
                                <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>      
                            </div><!-- End Item -->                                
                          </div><!-- End Carousel Inner -->
                        </div><!-- /.carousel -->
                        <li class="divider"></li>
                        <li><a href="catalogue.php">View all Collection <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                    </ul>
                  </li>
                  <li class="col-sm-3">
                    <ul>
                      <li class="dropdown-header">Dresses</li>
                      <li><a href="catalogue.php">Unique Features</a></li>
                      <li><a href="catalogue.php">Image Responsive</a></li>
                      <li><a href="catalogue.php">Auto Carousel</a></li>
                      <li><a href="catalogue.php">Newsletter Form</a></li>
                      <li><a href="catalogue.php">Four columns</a></li>
                      <li class="divider"></li>
                      <li class="dropdown-header">Tops</li>
                      <li><a href="catalogue.php">Good Typography</a></li>
                    </ul>
                  </li>
                  <li class="col-sm-3">
                    <ul>
                      <li class="dropdown-header">Jackets</li>
                      <li><a href="catalogue.php">Easy to customize</a></li>
                      <li><a href="catalogue.php">Glyphicons</a></li>
                      <li><a href="catalogue.php">Pull Right Elements</a></li>
                      <li class="divider"></li>
                      <li class="dropdown-header">Pants</li>
                      <li><a href="catalogue.php">Coloured Headers</a></li>
                      <li><a href="catalogue.php">Primary Buttons & Default</a></li>
                      <li><a href="catalogue.php">Calls to action</a></li>
                    </ul>
                  </li>
                  <li class="col-sm-3">
                    <ul>
                      <li class="dropdown-header">Accessories</li>
                      <li><a href="catalogue.php">Default Navbar</a></li>
                      <li><a href="catalogue.php">Lovely Fonts</a></li>
                      <li><a href="catalogue.php">Responsive Dropdown </a></li>             
                      <li class="divider"></li>
                      <li class="dropdown-header">Newsletter</li>
                      <form class="form" role="form">
                        <div class="form-group">
                          <label class="sr-only" for="email">Email address</label>
                          <input type="email" class="form-control" id="email" placeholder="Enter email">                                                              
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                      </form>                                                       
                    </ul>
                  </li>
                </ul>
            </li>
            <li><a href="about.php">PROFIL</a></li>
            <li><a href="blog.php">BLOG</a></li>
            <li><a href="contact.php">KONTAK</a></li>
            <li><a href="cart.php"><i class="fa fa-shopping-cart"></i> 2 item(s)</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>