<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Product List</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->
	<div class="container mt">
	<div class="row">
	        	<div class="col-lg-3 col-md-3 col-sm-12">

	        		<!-- Categories -->
	        		<div class="col-lg-12 col-md-12 col-sm-6">
		        		<div class="no-padding">
		            		<span class="title">CATEGORIES</span>
		            	</div>
						<div class="list-group list-categ">
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Morbi leo risus</a>
							<a href="catalogue.html" class="list-group-item">Porta ac consectetur ac</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
						</div>
					</div>
					<!-- End Categories -->

					<!-- Best Seller -->
					<div class="col-lg-12 col-md-12 col-sm-6">
						<div class="no-padding">
		            		<span class="title">BEST SELLER</span>
		            	</div>
						<div class="hero-feature">
			                <div class="thumbnail text-center">
			                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
			                    	<img src="images/product-1.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -13px; top: 0px;">
			                	</a>
			                    <div class="caption prod-caption">
			                        <h3><a href="details.php">Penn State College T-Shirt</a></h3>
			                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
			                        <p>
			                        	</p><div class="btn-group">
				                        	<a href="#" class="btn btn-default">IDR  528.96</a>
				                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
			                        	</div>
			                        <p></p>
			                    </div>
			                </div>
			            </div>
			            <div class="hero-feature hidden-sm">
			                <div class="thumbnail text-center">
			                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
			                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 280px; height: auto; max-width: none; max-height: none; left: -28px; top: 0px;">
			                	</a>
			                    <div class="caption prod-caption">
			                        <h3><a href="details.php">Ohio State College T-Shirt</a></h3>
			                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
			                        <p>
			                        	</p><div class="btn-group">
				                        	<a href="#" class="btn btn-default">IDR  924.25</a>
				                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
			                        	</div>
			                        <p></p>
			                    </div>
			                </div>
			            </div>
					</div>
					<!-- End Best Seller -->

	        	</div>

	        	<div class="clearfix visible-sm"></div>

				<!-- Featured -->
	        	<div class="col-lg-9 col-md-9 col-sm-12">
	        		<div class="col-lg-12 col-sm-12">
	            		<span class="title">PRODUCT LIST</span>
	            	</div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
			                        	<a href="#" class="btn btn-default">IDR  122.51</a>
			                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-3.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Classic Laundry Green Graphic T-Shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
			                        	<a href="#" class="btn btn-default">IDR  628.96</a>
			                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-4.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Disc Jockey Print T-Shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
			                        	<a href="#" class="btn btn-default">IDR  394.64</a>
			                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-5.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Live Nation 3 Days of Peace and Music</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
		                        		<a href="#" class="btn btn-default">IDR  428.96</a>
		                        		<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-6.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Live Nation ACDC Gray T-Shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
		                        		<a href="#" class="btn btn-default">IDR  428.96</a>
		                        		<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-7.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Live Nation Aerosmith Ivory T-Shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
		                        		<a href="#" class="btn btn-default">IDR  632.15</a>
		                        		<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<!-- End Featured -->

	        	<div class="clearfix visible-sm"></div>

				<!--  Category -->
	        	<div class="col-lg-9 col-md-9 col-sm-12">
					<div class="col-lg-12 col-sm-12">
	            		<span class="title"></span>
	            	</div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-10.jpg" alt="" style="position: absolute; width: 242px; height: 258px; max-width: none; max-height: none; left: 0px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"> Adorable Blue &amp; Red Striped Polo T-shirt </a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
			                        	<a href="#" class="btn btn-default">IDR  22.22</a>
			                        	<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-11.jpg" alt="" style="position: absolute; width: 248px; height: auto; max-width: none; max-height: none; left: -3px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"> Adorable Flame Black T-shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
		                        		<a href="#" class="btn btn-default">IDR  15.47</a>
		                        		<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-12.jpg" alt="" style="position: absolute; width: 251px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"> Adorable Red Printed T-shirt</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><div class="btn-group">
		                        		<a href="#" class="btn btn-default">IDR  20.63</a>
		                        		<a href="details.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        	</div>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="text-center">
			        	<ul class="pagination catalogue-pagination">
							<li class="disabled"><a>First</a></li>
							<li class="disabled"><a>Prev</a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">Next</a></li>
							<li><a href="#">Last</a></li>
						</ul>
					</div>
				</div>
				<!-- End  Category -->

	        </div>
	</div>

		<br><br>
	
<?php include "footer.php" ?>