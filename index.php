<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 HEADERWRAP
	 ***************************************************************************************************************** -->
	<div id="top-slider" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#top-slider" data-slide-to="0" class="active"></li>
	    <li data-target="#top-slider" data-slide-to="1"></li>
	    <li data-target="#top-slider" data-slide-to="2"></li>
	  </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
        <div class="item">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
        <div class="item">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
      </div>
      </div>


	<div class="container mt">
		
	<div class="row">	            		
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="well">
						<h4>Anda ingin update otomatis produk/jasa terbaru	dari bisnis ini?
							<div class="pull-right"><btn class="btn-warning btn-lg"><i class="fa fa-rss"></i> Yuk Update</btn></div>
						</h4>
					</div>
				</div>

	        	<div class="col-lg-3 col-md-3 col-sm-12">

	        		<!-- Categories -->
	        		<div class="col-lg-12 col-md-12 col-sm-6">
		        		<div class="no-padding">
		            		<span class="title">CATEGORIES</span>
		            	</div>
						<div class="list-group list-categ">
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Morbi leo risus</a>
							<a href="catalogue.html" class="list-group-item">Porta ac consectetur ac</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
						</div>
					</div>
					<!-- End Categories -->

					<!-- Categories -->
	        		<div class="col-lg-12 col-md-12 col-sm-6">
		        		<div class="no-padding">
		            		<span class="title">CATEGORIES</span>
		            	</div>
						<div class="list-group list-categ">
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Morbi leo risus</a>
							<a href="catalogue.html" class="list-group-item">Porta ac consectetur ac</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.html" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.html" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.html" class="list-group-item">Dapibus ac facilisis in</a>
						</div>
					</div>
					<!-- End Categories -->

				

	        	</div>

	        	<div class="clearfix visible-sm"></div>

				<!-- Featured -->
	        	<div class="col-lg-9 col-md-9 col-sm-9">

	        
	        		<div class="col-lg-12 col-sm-12">
	            		<span class="title">PRODUK TERBARU
	            			<div class="pull-right"><a class="btn btn-danger btn-sm">Baru</a><a class="btn btn-success btn-sm">Second</a></div>
	            		</span>
	            	</div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p>
		                        	<h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
	        	<!-- End Featured -->
	        	</div>
	        </div>
	</div>

		<br><br>
	
<?php include "footer.php" ?>